module PolinomisC1

using Polynomial

export arrels, q

arrels = p -> filter(isreal, roots(p))

function q(x)
   x^2
end

function f(x,y)

x + y

end

function mig(x, y)

(x + y )/2

end 


function derivative(f)
    return function(x)
        # pick a small value for h
        h = x == 0 ? sqrt(eps(Float64)) : sqrt(eps(Float64)) * x

        # floating point arithmetic gymnastics
        xph = x + h
        dx = xph - x

        # evaluate f at x + h
        f1 = f(xph)

        # evaluate f at x
        f0 = f(x)

        # divide the difference by h
        return (f1 - f0) / dx
    end


function quadratic(f)

    f1 = derivative(f)

    c = f(0.0)

    b = f1(0.0)

    a = f(1.0) - b - c

    return (-b + sqrt(b^2 - 4a*c + 0im))/2a, (-b - sqrt(b^2 - 4a*c + 0im))/2a

end

function tercergrau(f)

f1=derivative(f)

d=f(0.0)

c=f1(0.0)

f2=derivative(f1)

b=f2(0.0)

a=f(1.0) - b - c - d

return (-b/(3*a) - (2^(1/3)*(-b^2 + 3*a*c))/(3*a*(-2*b^3 + 9*a*b*c - 27*a^2*d + Sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)) + (-2*b^3 + 9*a*b*c - 27*a^2*d + Sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)/(3*2^(1/3)*a)), (-b/(3*a) + ((1 + i*Sqrt[3])*(-b^2 + 3*a*c))/ (3*2^(2/3)*a*(-2*b^3 + 9*a*b*c - 27*a^2*d + Sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)) - (1 - i*Sqrt[3])*(-2*b^3 + 9*a*b*c - 27*a^2*d + Sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)/(6*2^(1/3)*a)), (-b/(3*a) + ((1 - i*Sqrt[3])*(-b^2 + 3*a*c))/(3*2^(2/3)*a*(-2*b^3 + 9*a*b*c - 27*a^2*d + Sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)) - (1 + i*Sqrt[3])*(-2*b^3 + 9*a*b*c - 27*a^2*d + Sqrt[4*(-b^2 + 3*a*c)^3 + (-2*b^3 + 9*a*b*c - 27*a^2*d)^2])^(1/3)/(6*2^(1/3)*a))

end

end

end # module
