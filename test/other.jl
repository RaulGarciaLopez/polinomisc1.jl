using FactCheck

include("helpers.jl")

function is_subinterval_of(I)
	a,b = I 
	J -> J[1] >= a && J[2] <= b
end # is_subinterval_of

function width(I)
	@assert I[2] >= I[1]
	I[2] - I[1]
end # width

facts("Intervals") do

	I = (1, 2)
	f = x -> x^2 - 2

	@fact bisecta(f, I) => is_subinterval_of(I)
	@fact width(bisecta(f, I))/width(I) => roughly(1/2)

	@fact_throws bisecta(f, (1.5, 2))  # It doesn't contain a root.

end # facts