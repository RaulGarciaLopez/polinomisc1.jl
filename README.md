# Pràctica de Càlcul 1

## Objectiu

Construir una funció `arrels` que doni les arrels reals d'un polinomi. 

Aquesta funció i les altres auxiliars hauràn d'estar en un repositori de Bitbucket i tenir un format adient.

Aneu a [Wiki](https://bitbucket.org/jsaludes/polinomisc1.jl/wiki) per a més informació.




